jQuery(document).ready(function($){

	/**
	 * Кнопка наверх на jQuery.
	 * Автор: Тимур Камаев - wp-kama.ru
	 * version 2.2
	 */
	$('body').append('<style>\
		.scrollTop{\
			display:none; z-index:9999; position:fixed; bottom:10px; left:48%; width:50px; height:70px;\
		}\
		.scrollTop:hover{ background-position:0 -76px;}\
	</style>');

	var speed      = 500,
		$scrollTop = $('<a href="#" class="scrollTop"><i class="fa fa-chevron-up fa-2x" aria-hidden="true"></i></a>').appendTo('body');

	$scrollTop.click(function(e){
		e.preventDefault();

		$( 'html:not(:animated),body:not(:animated)' ).animate({ scrollTop: 0}, speed );
	});

	//появление
	function show_scrollTop(){
		( $(window).scrollTop() > 300 ) ? $scrollTop.stop().fadeIn(600) : $scrollTop.stop().fadeOut(600);
	}
	$(window).scroll( function(){ show_scrollTop(); } );
	show_scrollTop();

});
