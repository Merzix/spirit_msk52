# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from datetime import timedelta

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from ..core.conf import settings
from ..core.utils.models import AutoSlugField

EMPTY, MANAGEMENT_COMPANY_AGENT, SOVIET_HEAD, SOVIET_MEMBER = range(4)

POSITIONS = (
    (EMPTY, 'Нет'),
    (MANAGEMENT_COMPANY_AGENT, 'Представитель "УК Московский"'),
    (SOVIET_HEAD, 'Председатель совета многоквартирного дома'),
    (SOVIET_MEMBER, 'Член совета многоквартирного дома'),
)

FIO, FI, IF, IO, I, F, L, O, ILF, IL, FL, LF = range(12)

NAME_DISPLAY = (
    (FIO, 'Фамилия Имя Отчество'),
    (FI, 'Фамилия Имя'),
    (IF, 'Имя Фамилия'),
    (IO, 'Имя Отчество'),
    (I, 'Имя'),
    (F, 'Фамилия'),
    (L, 'Логин'),
    (O, 'Отчество'),
    (ILF, 'Имя Логин Фамилия'),
    (IL, 'Имя Логин'),
    (FL, 'Фамилия Логин'),
    (LF, 'Логин Фамилия'),
)

NAME_DISPLAY_DICT = {
    FIO: lambda f,i,o,l: ' '.join((f, i, o)),
    FI: lambda f,i,o,l: ' '.join((f, i)),
    IF: lambda f,i,o,l: ' '.join((i, f)),
    IO: lambda f,i,o,l: ' '.join((i, o)),
    I: lambda f,i,o,l: i,
    F: lambda f,i,o,l: f,
    L: lambda f,i,o,l: l,
    O: lambda f,i,o,l: o,
    ILF: lambda f,i,o,l: ' '.join((i, l, f)),
    IL: lambda f,i,o,l: ' '.join((i, l)),
    FL: lambda f,i,o,l: ' '.join((f, l)),
    LF: lambda f,i,o,l: ' '.join((l, f)),
}

def not_empty(str):
    return str and len(str) != str.count(' ')


class House(models.Model):
    number = models.CharField(_("house number"), max_length=5, unique=True)
    floors = models.SmallIntegerField(_("floors count"), blank=True)
    flats = models.IntegerField(_("flats count"), blank=True)
    company = models.CharField(_("management company"), max_length=255, null=True, blank=True)
    additional = models.TextField(_("additional information"), blank=True)

    def __str__(self):
        return self.number


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_("profile"), related_name='st')

    middle_name = models.CharField(_('middle name'), max_length=30, blank=True)
    display = models.SmallIntegerField(_('name display'), choices=NAME_DISPLAY, default=IF)
    slug = AutoSlugField(populate_from="user.username", db_index=False, blank=True)
    location = models.PositiveSmallIntegerField(_("apartment number"), null=True, blank=True, default=None)  # models.CharField(_("location"), max_length=3, blank=True)
    is_apartment_hidden = models.BooleanField(_('apartment hidden'), default=False)
    phone = models.CharField(_("phone number"), max_length=25, blank=True)
    is_phone_hidden = models.BooleanField(_('phone hidden'), default=True)
    special_status = models.SmallIntegerField(_('special status'), choices=POSITIONS, default=EMPTY)
    house = models.ForeignKey(House, verbose_name=_("house number"), related_name='occupants', blank=True, null=True)
    is_house_hidden = models.BooleanField(_('house hidden'), default=False)
    have_auto = models.BooleanField(_('have auto'), default=False)
    other_information = models.TextField(_('other information'), blank=True)
    last_seen = models.DateTimeField(_("last seen"), auto_now=True)
    last_ip = models.GenericIPAddressField(_("last ip"), blank=True, null=True)
    timezone = models.CharField(_("time zone"), max_length=32, default='Europe/Moscow')
    is_administrator = models.BooleanField(_('administrator status'), default=False)
    is_moderator = models.BooleanField(_('moderator status'), default=False)
    is_verified = models.BooleanField(_('verified'), default=False,
                                      help_text=_('Designates whether the user has verified his '
                                                  'account by email or by other means. Un-select this '
                                                  'to let the user activate his account.'))
    is_owner = models.BooleanField(_('owner status'), default=False)

    topic_count = models.PositiveIntegerField(_("topic count"), default=0)
    comment_count = models.PositiveIntegerField(_("comment count"), default=0)

    last_post_hash = models.CharField(_("last post hash"), max_length=32, blank=True)
    last_post_on = models.DateTimeField(_("last post on"), null=True, blank=True)

    class Meta:
        verbose_name = _("forum profile")
        verbose_name_plural = _("forum profiles")

    def save(self, *args, **kwargs):
        if self.user.is_superuser:
            self.is_administrator = True

        if self.is_administrator:
            self.is_moderator = True

        if settings.ST_OWNER_AUTO_CONFIRM and self.house:
            self.is_owner = True

        super(UserProfile, self).save(*args, **kwargs)

    def get_full_name(self):
        full_name = ' '.join((self.user.last_name, self.user.first_name, self.middle_name))
        return full_name if not_empty(full_name) else ''

    def get_display_name(self):
        name = NAME_DISPLAY_DICT[self.display](self.user.last_name, self.user.first_name, self.middle_name, self.user.username)
        return name if not_empty(name) else self.user.username

    def get_absolute_url(self):
        return reverse('spirit:user:detail', kwargs={'pk': self.user.pk, 'slug': self.slug})

    def get_status_display(self):
        if self.is_administrator:
            return _('Administrator')
        elif self.is_moderator:
            return _('Moderator')
        elif self.special_status:
            return POSITIONS[self.special_status][1]
        elif self.is_owner:
            return _('Owner')

    def update_post_hash(self, post_hash):
        assert self.pk

        # Let the DB do the hash
        # comparison for atomicity
        return bool(UserProfile.objects
                    .filter(pk=self.pk)
                    .exclude(
                        last_post_hash=post_hash,
                        last_post_on__gte=timezone.now() - timedelta(
                            minutes=settings.ST_DOUBLE_POST_THRESHOLD_MINUTES))
                    .update(
                        last_post_hash=post_hash,
                        last_post_on=timezone.now()))
