# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from ..models import UserProfile

User = get_user_model()


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ("username", "email", "is_active", "first_name", "last_name")

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = _('Login')



class UserProfileForm(forms.ModelForm):

    class Meta:
        model = UserProfile
        fields = ("middle_name", "display", "house", "is_house_hidden", "location", "is_apartment_hidden", "phone", "is_phone_hidden", "have_auto", "special_status", "is_owner", "other_information", "is_verified",
                  "is_administrator", "is_moderator")  # , "timezone"

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        self.fields['is_house_hidden'].disabled = True
        self.fields['is_apartment_hidden'].disabled = True
        self.fields['is_phone_hidden'].disabled = True
        self.fields['have_auto'].disabled = True
        self.fields['display'].disabled = True
