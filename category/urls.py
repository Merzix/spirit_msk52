# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.conf.urls import url

from . import views


urlpatterns = [
    # url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^$', views.index_active, name='index'),

    url(r'^(?P<pk>\d+)/$', views.detail, kwargs={'slug': "", }, name='detail'),
    url(r'^(?P<pk>\d+)/(?P<slug>[\w-]+)/$', views.detail, name='detail'),

    url(r'^struct/$', views.index_active, kwargs={'struct': True, }, name='struct_index'),
    url(r'^struct/(?P<pk>\d+)/$', views.detail, kwargs={'slug': "", 'struct': True, }, name='struct_detail'),
    url(r'^struct/(?P<pk>\d+)/(?P<slug>[\w-]+)/$', views.detail, kwargs={'struct': True, }, name='struct_detail'),
]