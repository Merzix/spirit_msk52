# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.views.generic import ListView
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponsePermanentRedirect

from djconfig import config

from ..core.utils.paginator import yt_paginate
from ..topic.models import Topic
from .models import Category


def detail(request, pk, slug, struct=False):
    user = request.user

    category = get_object_or_404(Category.objects.visible(user),
                                 pk=pk)

    if category.slug != slug:
        return HttpResponsePermanentRedirect(category.get_absolute_url())

    subcategories = Category.objects.visible(user).children(parent=category)

    if struct:
        for subc in subcategories:
            subc.topics = subc.st_topics\
                .visible(user)\
                .with_bookmarks(user=user)\
                .order_by('-is_globally_pinned', '-is_pinned', '-last_active')\

    topics = Topic.objects\
        .visible(user)\
        .with_bookmarks(user=user)\
        .for_category(category=category)\
        .order_by('-is_globally_pinned', '-is_pinned', '-last_active')\
        .select_related('category')

    topics = yt_paginate(
        topics,
        per_page=config.topics_per_page,
        page_number=request.GET.get('page', 1)
    )

    all_visible_categories = Category.objects\
        .parents()\
        .visible(user)\
        .exclude_category(category)

    for c in all_visible_categories:
        c.subcategories = Category.objects\
            .filter(parent=c)\
            .visible(user)

    context = {
        'all_visible_categories': all_visible_categories,
        'category': category,
        'subcategories': subcategories,
        'topics': topics,
        'struct': struct,
    }

    return render(request, 'spirit/category/detail.html', context)


# class IndexView(ListView):
#
#     template_name = 'spirit/category/index.html'
#     context_object_name = "categories"
#     queryset = Category.objects.visible().parents()


def index_active(request, struct=False):
    user = request.user

    categories = Category.objects.parents().visible(user)

    if struct:
        for cat in categories:
            cat.subcategories = cat.category_set.visible(user)
            cat.topics = Topic.objects \
                .for_category(category=cat) \
                .visible(user) \
                .order_by('-is_globally_pinned', '-is_pinned', '-last_active') \
                # .select_related('category')
            for subc in cat.subcategories:
                subc.topics = Topic.objects \
                    .for_category(category=subc) \
                    .visible(user) \
                    .order_by('-is_globally_pinned', '-is_pinned', '-last_active') \
                    # .select_related('category')

    context = {
        'categories': categories,
        'struct': struct,
    }

    return render(request, 'spirit/category/index.html', context)
