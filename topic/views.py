# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponsePermanentRedirect
from django.urls import reverse

from djconfig import config

from ..core.utils.paginator import paginate, yt_paginate
from ..core.utils.ratelimit.decorators import ratelimit
# from ..core.utils.decorators import guest_only
from ..category.models import Category
from ..comment.models import MOVED
from ..comment.forms import CommentForm
from ..comment.utils import comment_posted
from ..comment.models import Comment
from .models import Topic
from .forms import TopicForm  # , NoticeForm
from . import utils
from ..core.conf import settings


@login_required
@ratelimit(rate='1/10s')
def publish(request, category_id=None):
    user = request.user

    if category_id:
        get_object_or_404(
            Category.objects.visible(user),
            pk=category_id)

    if request.method == 'POST':
        form = TopicForm(user=user, data=request.POST)
        cform = CommentForm(user=user, data=request.POST)

        if (all([form.is_valid(), cform.is_valid()]) and
                not request.is_limited()):
            if not user.st.update_post_hash(form.get_topic_hash()):
                return redirect(
                    request.POST.get('next', None) or
                    form.get_category().get_absolute_url())

            # wrap in transaction.atomic?
            topic = form.save()
            cform.topic = topic
            comment = cform.save()
            comment_posted(comment=comment, mentions=cform.mentions)
            return redirect(topic.get_absolute_url())
    else:
        form = TopicForm(user=user, initial={'category': category_id})
        cform = CommentForm()

    context = {
        'form': form,
        'cform': cform,
    }

    return render(request, 'spirit/topic/publish.html', context)


# @guest_only
# @ratelimit(rate='1/10s')
# def add_notice(request):
#     user = User.objects.get(pk=settings.ST_UK_USER_PK)
#
#     if request.method == 'POST':
#         form = NoticeForm(user=user, data=request.POST)
#         cform = CommentForm(user=user, data=request.POST)
#
#         if (all([form.is_valid(), cform.is_valid()]) and
#                 not request.is_limited()):
#             if user.st.update_post_hash(form.get_topic_hash()):
#                 # wrap in transaction.atomic?
#                 topic = form.save()
#                 cform.topic = topic
#                 comment = cform.save()
#                 comment_posted(comment=comment, mentions=cform.mentions)
#             return redirect('/')
#     else:
#         form = NoticeForm(user=user)
#         cform = CommentForm()
#
#     context = {
#         'form': form,
#         'cform': cform,
#     }
#
#     return render(request, 'spirit/topic/publish.html', context)


@login_required
def update(request, pk):
    topic = Topic.objects.for_update_or_404(pk, request.user)

    if request.method == 'POST':
        form = TopicForm(user=request.user, data=request.POST, instance=topic)
        category_id = topic.category_id

        if form.is_valid():
            topic = form.save()

            if topic.category_id != category_id:
                Comment.create_moderation_action(user=request.user, topic=topic, action=MOVED)

            return redirect(request.POST.get('next', topic.get_absolute_url()))
    else:
        form = TopicForm(user=request.user, instance=topic)

    context = {
        'form': form,
    }

    return render(request, 'spirit/topic/update.html', context)


def detail(request, pk, slug):
    user = request.user

    topic = Topic.objects.get_public_or_404(pk, user)

    if topic.slug != slug:
        return HttpResponsePermanentRedirect(topic.get_absolute_url())

    utils.topic_viewed(request=request, topic=topic)

    comments = Comment.objects\
        .for_topic(topic=topic)\
        .with_likes(user=user)\
        .with_polls(user=user)\
        .order_by('date')

    comments = paginate(
        comments,
        per_page=config.comments_per_page,
        page_number=request.GET.get('page', 1)
    )

    context = {
        'topic': topic,
        'comments': comments
    }

    return render(request, 'spirit/topic/detail.html', context)


def index_active(request):
    user = request.user

    categories = Category.objects\
        .visible(user)\
        .parents()
    for category in categories:
        category.subcategories = Category.objects.filter(parent=category).visible(user)

    topics = Topic.objects \
        .visible(user) \
        .global_() \
        .with_bookmarks(user) \
        .order_by('-is_globally_pinned', '-last_active') \
        .select_related('category')

    topics = yt_paginate(
        topics,
        per_page=config.topics_per_page,
        page_number=request.GET.get('page', 1)
    )

    context = {
        'categories': categories,
        'topics': topics,
    }

    return render(request, 'spirit/topic/active.html', context)


@login_required
def new(request):
    user = request.user

    categories = Category.objects\
        .visible(user)\
        .parents()
    for category in categories:
        category.subcategories = Category.objects.filter(parent=category).visible(user)

    topics = Topic.objects \
        .visible(user) \
        .filter(last_active__gte=user.st.last_seen)\
        .global_() \
        .with_bookmarks(user) \
        .order_by('-is_globally_pinned', '-last_active') \
        .select_related('category')

    topics = yt_paginate(
        topics,
        per_page=config.topics_per_page,
        page_number=request.GET.get('page', 1)
    )

    context = {
        'categories': categories,
        'topics': topics,
        'last_seen': True,
    }

    return render(request, 'spirit/topic/active.html', context)
